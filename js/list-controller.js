"use strict";

let switched = false;
let param = 'name';

list_switch.addEventListener('click', setupList);
let listContainer = document.querySelector('.stock-list');


function setupList() {
    document.querySelector('.additional-block').style.display = 'none';
    switched = true;
    document.querySelector('.stock-grid').style.display = 'none';
    list_switch.parentElement.classList.add('active');
    grid_switch.parentElement.classList.remove('active');

    document.querySelector('.misc-controls').innerHTML = '<div class="sort-options">\n' +
        '    <button class="sort-button sort-name">Sort by name <i class="fas fa-long-arrow-alt-up"></i></button>\n' +
        '    <button class="sort-button sort-price">Sort by price <i class="fas fa-long-arrow-alt-down"></i></button>\n' +
        '</div>';

    nameButton = document.querySelector('.sort-name');
    priceButton = document.querySelector('.sort-price');
    nameButton.addEventListener('click', sortListName);
    priceButton.addEventListener('click', sortListPrice);
    listInit();
}

function listInit() {
    listContainer.innerHTML = '';
    for (let [key,value] of pizzaMapTemp)
    {
        let listItem = document.createElement('div');
        listItem.classList.add('list-item');

        let name = document.createElement('div');
        name.classList.add('list-name');
        name.innerHTML = `<i class="fas fa-pizza-slice list-icon"></i> &nbsp;${key}`;

        let price = document.createElement('div');
        price.classList.add('list-price');
        price.innerHTML = `<i class="fas fa-dollar-sign list-dollar"></i>&nbsp;${(value.price+value.init_price).toFixed(2)}`;

        let buttons = document.createElement('div');
        buttons.classList.add('list-buttons');
        let details = document.createElement('button');
        let cart = document.createElement('button');
        cart.classList.add('list-cart');
        cart.innerHTML = '<i class="fas fa-cart-plus"></i> &nbsp;Add to cart';
        buttons.appendChild(cart);

        listItem.appendChild(name);
        listItem.appendChild(price);
        listItem.appendChild(buttons);

        listContainer.appendChild(listItem);
    }
}