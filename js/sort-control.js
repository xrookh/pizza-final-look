"use strict";

let searchButton;
let searchInput;
let filters = document.querySelector('.filters');
filters.addEventListener('click', deleteFilter);

let sortNameFlag = true;
let sortPriceFlag = true;
let nameButton;
let priceButton;

function searchIngredient() {
    let str = searchInput.value;
    searchInput.value = '';
    if (!ingredientsMap.has(str.toLowerCase()) ) {alert('No such ingredient!');}
    else
    {
        let newFilter = document.createElement('div');
        newFilter.classList.add('search-filter');
        newFilter.innerHTML = ''+str.toLowerCase() + '&nbsp;&nbsp;<i class="fas fa-times"></i>';
        filters.appendChild(newFilter);
        updateSearch();

    }
}

function updateSearch() {

    let options = filters.children;
    let searchSet = new Set();
    for (let i=0; i<options.length; i++)
    {
        let text = options[i].textContent.slice(0,options[i].textContent.length-2);
        if (searchSet.has(text)) {options[i].parentElement.removeChild(options[i]);}
        else searchSet.add(text);
    }
    let cards = gridContainer.children;
    for (let i=0; i<cards.length; i++)
    {
        let spans = cards[i].querySelectorAll('.front-ingredient');
        let counter = 0;
        for (let j=0; j<spans.length; j++)
        {
            if (searchSet.has(spans[j].innerHTML)) counter++;
        }
        if (counter<searchSet.size) cards[i].classList.add('hide');
        else cards[i].classList.remove('hide');
    }
}

function deleteFilter(e) {
    if (e.target.tagName.toLowerCase() === 'i') e.target.parentElement.parentElement.removeChild(e.target.parentElement);
    else e.target.parentElement.removeChild(e.target);
    updateSearch();
}

function sortListName()
{
    if (sortNameFlag)
    {
        param = 'name';
        pizzaMapTemp = new Map(Array.from(pizzaMapTemp).sort(sortListDescending));
        listInit();
        nameButton.innerHTML = 'Sort by name <i class="fas fa-long-arrow-alt-down"></i>';
        sortNameFlag = false;
    } else
    {
        param = 'name';
        pizzaMapTemp = new Map(Array.from(pizzaMapTemp).sort(sortListAscending));
        listInit();
        nameButton.innerHTML = 'Sort by name <i class="fas fa-long-arrow-alt-up"></i>';
        sortNameFlag = true;
    }
}
function sortListPrice() {
    if (sortPriceFlag)
    {
        param = 'price';
        pizzaMapTemp = new Map(Array.from(pizzaMapTemp).sort(sortListDescending));
        listInit();
        priceButton.innerHTML = 'Sort by price <i class="fas fa-long-arrow-alt-down"></i>';
        sortPriceFlag = false;
    } else
    {
        param = 'price';
        pizzaMapTemp = new Map(Array.from(pizzaMapTemp).sort(sortListAscending));
        listInit();
        priceButton.innerHTML = 'Sort by price <i class="fas fa-long-arrow-alt-up"></i>';
        sortPriceFlag = true;
    }
}

function sortListAscending(a, b)
{
    if (param === 'name') {if (a[0] > b[0]) return 1; else return -1;}
    else {if (a[1][param] > b[1][param]) return 1; else return -1;}

}
function sortListDescending(a,b) {
    if (param === 'name') {if (a[0] < b[0]) return 1; else return -1;}
    else {if (a[1][param] < b[1][param]) return 1; else return -1;}
}
