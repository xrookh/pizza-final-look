"use strict";
if (localStorage.getItem('cart') === null) localStorage.setItem('cart', JSON.stringify([]));

let cartOpen = document.querySelector('.cart-open');
let cartContainer = document.querySelector('.cart-container');
let cartClear = document.querySelector('.cart-clear');
let cartCheckout = document.querySelector('.modal-button-wrap .cart-button');
cartClear.addEventListener('click', clearCart);
cartCheckout.addEventListener('click', spiderMan);
updateCart();
cartOpen.addEventListener('click', openCart);

function openCart() {
    cartModal.style.display = 'block';
}
function addToCart(obj) {
    let buff = JSON.parse(localStorage.getItem('cart'));
    let newItem = true;
    for (let i=0; i<buff.length; i++)
    {
        if (buff[i].name === obj.name && isSetEqual(new Set(buff[i].ingredients), obj.ingredients))
        {
            buff[i].amount++;
            newItem = false;
            i = buff.length;
        }
    }
    if (newItem)
    {
        obj.amount = 1;
        buff.push(obj);
    }
    localStorage.setItem('cart', JSON.stringify(buff));
    updateCart();

}
function updateCart() {
    let buff = JSON.parse(localStorage.getItem('cart'));
    document.querySelector('.cart-badge').textContent = buff.length;
    cartContainer.innerHTML = '';
    for (let i=0; i<buff.length; i++) {
        let card = document.createElement('div');
        card.classList.add('cart-pizza-card');

        let header = document.createElement("div");
        header.classList.add('cart-header');
        header.textContent = `${buff[i].name}`;

        let price = document.createElement("div");
        price.classList.add('cart-price');
        price.innerHTML = `<i class="fas fa-dollar-sign list-dollar"></i>&nbsp;${buff[i].price}`;

        let amount = document.createElement("div");
        amount.classList.add('cart-amount', 'number-input');
        amount.innerHTML = `<button onclick="this.parentNode.querySelector('input').stepDown()" class="left">-</button>
         <input class="quantity" min="0" value="${buff[i].amount}" max="10" type="number">
         <button onclick="this.parentNode.querySelector('input').stepUp()"class="right">+</button>`;

        let hr = document.createElement("hr");
        hr.classList.add('card-hr');
        card.appendChild(header);
        card.appendChild(price);
        card.appendChild(amount);
        cartContainer.appendChild(card);
        cartContainer.appendChild(hr);
    }
}
function isSetEqual(set, arr) {
    for (let i=0; i<arr.length; i++)
    {
        if (!set.has(arr[i])) return false;
    }
    return true;
}

function clearCart() {
    localStorage.setItem('cart', JSON.stringify([]));
    updateCart();
}
function spiderMan() {
    window.open('https://www.youtube.com/watch?v=TRgdA9_FsXM');
    localStorage.setItem('cart', JSON.stringify([]));
    updateCart();
}