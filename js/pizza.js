"use strict";
if (localStorage.getItem('customPizzas')===null){localStorage.setItem('customPizzas', JSON.stringify([]))}

let ingredientsMap = new Map([
    ['tomato', {price:0.25, cal:20}],
    ['basil', {price:0.1, cal:5}],
    ['sausage',{price: 0.5, cal:50}],
    ['ham',{price:0.5, cal:50}],
    ['chicken',{price:0.5, cal:47}],
    ['beef',{price:0.75, cal:68}],
    ['pork',{price:0.75, cal:72}],
    ['sous-vide',{price: 1, cal: 40}],
    ['bbq',{price:0.25, cal:40}],
    ['mozzarella',{price:0.5, cal:50}],
    ['garlic',{price:0.1, cal: 10}],
    ['paprika',{price:0.1, cal:10}],
    ['pepper',{price:0.1, cal:10}],
    ['marinara',{price:0.25, cal:10}],
    ['pesto',{price: 0.15, cal: 10}],
    ['mushrooms',{price: 0.15, cal: 20}],
    ['cheddar',{price: 0.5, cal: 40}],
    ['parmesan',{price: 1, cal: 45}],
    ['dor-blue',{price: 1.25, cal: 45}],
    ['pepperoni',{price: 0.5, cal: 50}],
    ['salami',{price: 0.5, cal: 35}],
    ['onion',{price: 0.15, cal: 10}],
    ['cucumber',{price: 0.15, cal: 10}],
    ['parsley',{price: 0.1, cal: 5}]
]);

let pizzaMap = new Map([
    ['Americano',{
        ingredients: new Set(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/americano.png",
        front_photo_name:"./resources/img/pizza/front-americano.png",
        init_price:3,
        init_cal:200
    }],
    ["Americano XXL",{
        ingredients: new Set(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/americano-xxl.png",
        front_photo_name:"./resources/img/pizza/front-americano-xxl.png",
        init_price:6,
        init_cal:400
    }],
    [ "Bavarian",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/bavarian.png",
        front_photo_name:"./resources/img/pizza/front-bavarian.png",
        init_price:3,
        init_cal:200
    }],
    ["Bavarian XXL",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/bavarian-xxl.png",
        front_photo_name:"./resources/img/pizza/front-bavarian-xxl.png",
        init_price:6,
        init_cal:400
    }],
    ["Berlusconi XXL",{
        ingredients: new Set(['salami', 'chicken','mushrooms','mozzarella', 'garlic', 'parsley', 'basil']),
        price:getResult(['salami', 'chicken','mushrooms','mozzarella', 'garlic', 'parsley', 'basil'], 'price'),
        calories:getResult(['salami', 'chicken','mushrooms','mozzarella', 'garlic', 'parsley', 'basil'], 'cal'),
        photo_name:"./resources/img/pizza/berlusconi-xxl.png",
        front_photo_name:"./resources/img/pizza/front-berlusconi-xxl.png",
        init_price:6,
        init_cal:400
    }],
    ["Calzzone",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'parmesan']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'parmesan'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'parmesan'], 'cal'),
        photo_name:"./resources/img/pizza/calzzone.png",
        front_photo_name:"./resources/img/pizza/front-calzzone.png",
        init_price:3,
        init_cal:200
    }],
    ["Carbonara",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/carbonara.png",
        front_photo_name:"./resources/img/pizza/front-carbonara.png",
        init_price:3,
        init_cal:200
    }],
    ["Carbonara XXL",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/carbonara-xxl.png",
        front_photo_name:"./resources/img/pizza/front-carbonara-xxl.png",
        init_price:6,
        init_cal:400
    }],
    ["Goro",{
        ingredients: new Set(['tomato', 'ham','mushrooms','cheddar', 'beef', 'pesto']),
        price:getResult(['tomato', 'ham','mushrooms','cheddar', 'beef', 'pesto'], 'price'),
        calories:getResult(['tomato', 'ham','mushrooms','cheddar', 'beef', 'pesto'], 'cal'),
        photo_name:"./resources/img/pizza/cheese.png",
        front_photo_name:"./resources/img/pizza/front-goro.png",
        init_price:3,
        init_cal:200
    }],
    ["Diablo",{
        ingredients: new Set(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara']),
        price:getResult(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/diablo.png",
        front_photo_name:"./resources/img/pizza/front-diablo.png",
        init_price:3,
        init_cal:200
    }],
    ["Diablo XXL",{
        ingredients: new Set(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara']),
        price:getResult(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'mozzarella', 'pepperoni', 'garlic', 'pepper', 'paprika', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/diablo-xxl.png",
        front_photo_name:"./resources/img/pizza/front-diablo-xxl.png",
        init_price:6,
        init_cal:400
    }],
    ["Gurmeo",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork'], 'cal'),
        photo_name:"./resources/img/pizza/gurmeo.png",
        front_photo_name:"./resources/img/pizza/front-gurmeo.png",
        init_price:3,
        init_cal:200
    }],
    ["Gurmeo XXL",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms', 'sausage', 'dor-blue', 'pork'], 'cal'),
        photo_name:"./resources/img/pizza/gurmeo-xxl.png",
        front_photo_name:"./resources/img/pizza/front-gurmeo-xxl.png",
        init_price:6,
        init_cal:400
    }],
    ["Hunters` treasure",{
        ingredients: new Set(['bbq', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork']),
        price:getResult(['bbq', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork'], 'price'),
        calories:getResult(['bbq', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork'], 'cal'),
        photo_name:"./resources/img/pizza/hunter.png",
        front_photo_name:"./resources/img/pizza/front-hunter.png",

        init_price:3,
        init_cal:200
    }],
    ["Hunters` treasure XXL",{
        ingredients: new Set(['bbq', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork']),
        price:getResult(['bbq', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork'], 'price'),
        calories:getResult(['bbq', 'ham','sausage','mozzarella', 'pepperoni', 'beef', 'parsley', 'basil', 'pork'], 'cal'),
        photo_name:"./resources/img/pizza/hunter-xxl.png",
        front_photo_name:"./resources/img/pizza/front-hunter-xxl.png",
        init_price:6,
        init_cal:400
    }],
    ["Mafia",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/mafia.png",
        front_photo_name:"./resources/img/pizza/front-mafia.png",
        init_price:3,
        init_cal:200
    }],

    ["Mafia XXL",{
        ingredients: new Set(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','mushrooms','mozzarella', 'pepperoni', 'garlic', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/mafia-xxl.png",
        front_photo_name:"./resources/img/pizza/front-mafia-xxl.png",
        init_price:6,
        init_cal:400
    }],

    ["Margarita",{
        ingredients: new Set(['tomato', 'ham', 'parsley', 'basil', 'marinara']),
        price:getResult(['tomato', 'ham', 'parsley', 'basil', 'marinara'], 'price'),
        calories:getResult(['tomato', 'ham', 'parsley', 'basil', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/margarita.png",
        front_photo_name:"./resources/img/pizza/front-margarita.png",
        init_price:3,
        init_cal:200
    }],

    ["Polo",{
        ingredients: new Set(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara']),
        price:getResult(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara'], 'price'),
        calories:getResult(['tomato', 'bbq', 'ham','cucumber','mozzarella', 'parsley', 'pesto', 'marinara'], 'cal'),
        photo_name:"./resources/img/pizza/polo.png",
        front_photo_name:"./resources/img/pizza/front-polo.png",
        init_price:3,
        init_cal:200
    }]
]);

let init_custom_buffer = JSON.parse(localStorage.getItem('customPizzas'));
for (let i=0; i<init_custom_buffer.length; i++)
{
    init_custom_buffer[i].ingredients = new Set(init_custom_buffer[i].ingredients);
    pizzaMap.set(init_custom_buffer[i].name, init_custom_buffer[i]);
}

let pizzaMapTemp = new Map(pizzaMap);

function getResult(set, param) {
    let result = 0;
    for (let i=0; i<set.length; i++)
    {
        result += ingredientsMap.get(set[i])[param];
    }
    return result;
}