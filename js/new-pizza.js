"use strict";
let modal = document.querySelector(".create-pizza-modal");
let cartModal = document.querySelector('.cart-modal');
let open = document.querySelector(".create-new");
let close = document.querySelector(".pizza-modal-close");
let sizePick = document.querySelector(".modal-size-picker");
let ingredientPick = document.querySelector(".modal-ingredients-picker");
ingredientPick.addEventListener('click', updateSelection);
let started = false;
let isXXL = false;
let pizza_init_price = 3;
let pizza_init_cal = 200;
let pizzaPrice = 3;
let pizzaCal = 200;
let pizzaIngredients;

window.onclick = function(event) {
    if (event.target === modal) { closeModal(false); } else
    if (event.target === cartModal) {cartModal.style.display = 'none';}
};
sizePick.onclick = function (e)
{
    if(e.target.classList.contains('modal-switch-xxl'))
    {
        pizza_init_cal = 400;
        pizza_init_price = 6;
        pizzaPrice = 6;
        pizzaCal = 400;
        isXXL = true;
    }
    started = true;
    sizePick.style.animation = 'fadeOut .3s';
    setTimeout(() => {sizePick.style.display = 'none'; ingredientsPickerInit()}, 300);
};
open.onclick = function() {
    modal.style.display = "block";
};
function updateSelection(e) {
    if(e.target.classList.contains('ingredient-selector'))
    {
        if(e.target.classList.contains('active-ingredient')) {e.target.classList.remove('active-ingredient'); recalculateModal();}
        else {e.target.classList.add('active-ingredient'); recalculateModal();}
    } else if (e.target.classList.contains('pizza-discard')) {closeModal(false);}
    else if (e.target.classList.contains('pizza-submit'))
    {
        if(new_name.value === '')
        {
            alert("Give your masterpiece a name, fella!");
        } else
        {
            let buff = JSON.parse(localStorage.getItem('customPizzas'));

            let temp = {};
            temp.name = isXXL? new_name.value.trim() + ' XXL': new_name.value.trim();
            temp.price = pizzaPrice - pizza_init_price;
            temp.calories = pizzaCal;
            temp.init_price = pizza_init_price;
            temp.init_cal = pizza_init_cal;
            temp.ingredients = pizzaIngredients;
            temp.photo_name = isXXL? "./resources/img/pizza/mafia-xxl.png":"./resources/img/pizza/mafia.png";
            temp.front_photo_name = "./resources/img/pizza/custom.png";
            buff.push(temp);
            localStorage.setItem('customPizzas', JSON.stringify(buff));
            temp.ingredients = new Set(pizzaIngredients);
            pizzaMap.set(temp.name, temp);
            pizzaMapTemp.set(temp.name, temp);
            closeModal(true);
            setupGrid();
            scrollTo(0, document.body.scrollHeight);
        }
    }
}

function recalculateModal() {
    let ingredients = ingredientPick.querySelectorAll(".active-ingredient");
    let newSet = [];
    pizzaPrice = pizza_init_price;
    pizzaCal = pizza_init_cal;
    for (let i=0; i<ingredients.length; i++)
    {
        newSet.push(ingredients[i].innerHTML);
        pizzaPrice += ingredientsMap.get(ingredients[i].innerHTML).price;
        pizzaCal += ingredientsMap.get(ingredients[i].innerHTML).cal;
    }
    pizzaIngredients = newSet;
    ingredientPick.querySelector('.price').innerHTML = `<i class="fas fa-dollar-sign"></i>&nbsp;${pizzaPrice.toFixed(2)}`;
    ingredientPick.querySelector('.calories').innerHTML = `<i class="fas fa-pizza-slice"></i> &nbsp;${pizzaCal} cal<span class="small">/100g</span>`;
}

function closeModal(ask) {
    if(started)
    {
        if (ask || confirm("Discard your beautiful pizza?"))
        {
            modal.style.display = "none";
            started = false;
            isXXL = false;
            pizzaPrice = 3;
            pizzaCal = 200;
            sizePick.style.display = 'flex';
            sizePick.style.animation = '';
        }
    } else
    {
        modal.style.display = "none";
    }
}
function ingredientsPickerInit() {
    ingredientPick.innerHTML = `<div class="modal-name-input">
                            <label for="new_name"><b>Name your pizza: </b></label>
                            <input type="text" maxlength="20" id="new_name">
                        </div>
                        <div class="sauce-pick">Choose sauces:
                            <div class="ingredient-selector">tomato</div>
                            <div class="ingredient-selector">pesto</div>
                            <div class="ingredient-selector">bbq</div>
                            <div class="ingredient-selector">marinara</div>
                        </div>
                        <hr class="card-hr">
                        <div class="meat-pick">Choose meat:
                            <div class="ingredient-selector">chicken</div>
                            <div class="ingredient-selector">beef</div>
                            <div class="ingredient-selector">pork</div>
                            <div class="ingredient-selector">salami</div>
                            <div class="ingredient-selector">sausage</div>
                            <div class="ingredient-selector">ham</div>
                            <div class="ingredient-selector">pepperoni</div>
                        </div>
                        <hr class="card-hr">
                        <div class="cheese-pick">Choose cheese:
                            <div class="ingredient-selector">cheddar</div>
                            <div class="ingredient-selector">mozzarella</div>
                            <div class="ingredient-selector">parmesan</div>
                            <div class="ingredient-selector">dor-blue</div>
                        </div>
                        <hr class="card-hr">
                        <div class="misc-pick">Choose additional toppings:
                            <div class="ingredient-selector">basil</div>
                            <div class="ingredient-selector">garlic</div>
                            <div class="ingredient-selector">paprika</div>
                            <div class="ingredient-selector">pepper</div>
                            <div class="ingredient-selector">mushrooms</div>
                            <div class="ingredient-selector">onion</div>
                            <div class="ingredient-selector">cucumber</div>
                            <div class="ingredient-selector">parsley</div>
                        </div>
                        <div class="new-pizza-info">
                            <div class="card-info modal-info">
                                <div class="price"><i class="fas fa-dollar-sign"></i>&nbsp;${pizzaPrice}</div>
                                <div class="calories"><i class="fas fa-pizza-slice"></i> &nbsp;${pizzaCal} cal<span class="small">/100g</span></div>
                            </div>
                        </div>
                        <div class="modal-button-wrap">
                            <button class="pizza-discard pizza-button">Discard <i class="far fa-times-circle"></i></button>
                            <button class="pizza-submit pizza-button">Submit <i class="fas fa-plus-circle"></i></button>
                        </div>`
}