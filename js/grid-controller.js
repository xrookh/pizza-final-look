"use strict";

grid_switch.addEventListener('click', setupGrid);
let gridContainer = document.querySelector('.stock-grid');
let misc = document.querySelector('.misc-controls');

setupGrid();

function setupGrid()
{
        document.querySelector('.additional-block').style.display = 'flex';
        document.querySelector('.stock-list').innerHTML = '';
        grid_switch.parentElement.classList.add('active');
        list_switch.parentElement.classList.remove('active');

        misc.innerHTML = '<input type="text" maxlength="20" list="ingredients" class="search" placeholder="Search...">';
        let datalist = document.createElement('datalist');
        datalist.id = 'ingredients';
        for (let key of ingredientsMap.keys())
        {
            let temp = document.createElement("option");
            temp.value = ''+key;
            datalist.appendChild(temp);
        }
        misc.appendChild(datalist);

        let button = document.createElement("button");
        button.classList.add('search-submit');
        button.innerHTML = '<i class="fas fa-plus"></i>';
        misc.appendChild(button);

        searchButton = document.querySelector('.search-submit');
        searchButton.onclick = searchIngredient;
        searchInput = document.querySelector('.search');

        if (switched) {gridContainer.style.display = 'flex'} else gridInit();
        updateSearch();
}

function gridInit() {
    for (let [key,value] of pizzaMap)
    {
        let card = document.createElement('div');
        card.classList.add('card');

        let front = document.createElement('div');
        front.classList.add('card-front', 'face');

        let frontPhoto = document.createElement('div');
        frontPhoto.classList.add('card-photo');
        frontPhoto.style.backgroundImage = `url(${value.front_photo_name})`;

        let header = document.createElement('h3');
        header.classList.add('card-header');
        header.innerHTML = `${key}`;

        let hr = document.createElement('hr');
        hr.classList.add('card-hr');

        let ingredients = document.createElement('div');
        ingredients.classList.add('card-ingredients');
        let text = document.createElement('p');
        text.innerHTML = 'Ingredients: '

        let arr = Array.from(value.ingredients);
        for (let j=0; j<arr.length; j++)
        {
            let span = document.createElement('span');
            span.classList.add('front-ingredient');
            span.innerHTML = ''+ arr[j];
            text.appendChild(span);
            text.innerHTML += ', ';
        }
        text.innerHTML = text.innerHTML.slice(0,text.innerHTML.length-2);
        ingredients.appendChild(text);

        let info = document.createElement('div');
        info.classList.add('card-info');
        let price = document.createElement('div');
        price.classList.add('price');
        price.innerHTML = `<i class="fas fa-dollar-sign"></i> ${(value.price+value.init_price).toFixed(2)}`;

        let calorie = document.createElement('div');
        calorie.classList.add('calories');
        calorie.innerHTML = `<i class="fas fa-pizza-slice"></i> ${value.calories+value.init_cal} cal<span class="small">/100g</span>`;
        info.appendChild(price);
        info.appendChild(calorie);


        let buttonWrap = document.createElement('div');
        buttonWrap.classList.add('card-button-wrap');
        let details = document.createElement('button');
        details.classList.add('pizza-button', 'details-button');
        details.innerHTML = `Customize <i class="fas fa-ellipsis-h"></i>`;
        buttonWrap.appendChild(details);
        let cart = document.createElement('button');
        cart.classList.add('pizza-button', 'cart-button');
        cart.innerHTML = `Add to cart <i class="fas fa-cart-plus"></i>`;
        buttonWrap.appendChild(cart);

        let backHR = document.createElement('div');
        backHR.classList.add('card-hr');
        front.appendChild(frontPhoto);
        front.appendChild(header);
        front.appendChild(backHR);
        front.appendChild(ingredients);
        front.appendChild(info);
        front.appendChild(buttonWrap);


        let back = document.createElement('div');
        back.classList.add('card-back', 'face');

        let backPhoto = document.createElement('div');
        backPhoto.classList.add('back-photo');
        backPhoto.style.background = `url(${value.photo_name}) -30px -40px`;

        let backHeader = document.createElement('h3');
        backHeader.classList.add('card-header');
        backHeader.innerHTML = 'Pick ingredients:';

        let ingredientsPicker = document.createElement('div');
        ingredientsPicker.classList.add('ingredients-picker');
        let temp = Array.from(value.ingredients);

        for (let j=0; j<temp.length; j++)
        {
            let ingr = document.createElement('div');
            ingr.classList.add('ingredient-selector', 'active-ingredient');
            ingr.innerHTML = ''+temp[j];
            ingredientsPicker.appendChild(ingr);
        }

        for (let key of ingredientsMap.keys())
        {
            if(!value.ingredients.has(key))
            {
                let ingr = document.createElement('div');
                ingr.classList.add('ingredient-selector');
                ingr.innerHTML = ''+key;
                ingredientsPicker.appendChild(ingr);
            }
        }


        let backInfo = document.createElement('div');
        backInfo.classList.add('back-info');
        let backPrice = document.createElement('div');
        backPrice.classList.add('price');
        backPrice.innerHTML = `<i class="fas fa-dollar-sign"></i> ${(value.price+value.init_price).toFixed(2)}`;
        let backCalorie = document.createElement('div');
        backCalorie.classList.add('calories');
        backCalorie.innerHTML = `<i class="fas fa-pizza-slice"></i> ${value.calories} cal<span class="small">/100g</span>`;
        backInfo.appendChild(backPrice);
        backInfo.appendChild(backCalorie);

        let backButtonWrap = document.createElement('div');
        backButtonWrap.classList.add('back-button-wrap');
        let backButton = document.createElement('button');
        backButton.classList.add('pizza-button','back-button');
        backButton.innerHTML = `Back <i class="fas fa-long-arrow-alt-left"></i>`;
        let backCart = document.createElement('button');
        backCart.classList.add('pizza-button', 'cart-button');
        backCart.innerHTML = `Add to cart <i class="fas fa-cart-plus"></i>`;

        backButtonWrap.appendChild(backButton);
        // backButtonWrap.appendChild(backCart);

        back.appendChild(backPhoto);
        back.appendChild(backHeader);
        back.appendChild(hr);
        back.appendChild(ingredientsPicker);
        back.appendChild(backInfo);
        back.appendChild(backButtonWrap);

        card.appendChild(front);
        card.appendChild(back);
        card.addEventListener('click', clickHandler);
        gridContainer.appendChild(card);
    }
}

function clickHandler(e) {
    if (e.target.classList.contains('details-button'))
    {
        e.target.parentNode.parentNode.parentElement.style.transform = 'rotateY(180deg)';
        e.target.parentNode.parentNode.parentElement.style.boxShadow = '-7px 7px 10px #0f1219';
    } else if (e.target.classList.contains('back-button'))
    {
        e.target.parentNode.parentNode.parentElement.style.transform = 'rotateY(0deg)';
        e.target.parentNode.parentNode.parentElement.style.boxShadow = '7px 7px 10px #0f1219';
    } else if (e.target.classList.contains('ingredient-selector'))
    {
        if (e.target.classList.contains('active-ingredient'))
        {
            e.target.classList.remove('active-ingredient');
            recalculateInfo(e.target);
        } else
        {
            e.target.classList.add('active-ingredient');
            recalculateInfo(e.target);
        }
    } else if(e.target.classList.contains('cart-button'))
    {
        let object = {};
        object.name = e.target.parentElement.parentElement.querySelector('.card-header').textContent;
        object.price = parseFloat(e.target.parentElement.parentElement.querySelector('.price').textContent);
        object.ingredients = [];
        let ingredients = e.target.parentElement.parentElement.parentElement.querySelectorAll('.active-ingredient');
        for (let i=0; i<ingredients.length; i++)
        {
            object.ingredients.push(ingredients[i].textContent);
        }
        addToCart(object);
    }
}

function recalculateInfo(element) {
    let back_info = element.parentNode.parentElement.querySelector('.back-info');
    let front_info = element.parentNode.parentNode.parentElement.querySelector('.card-front').querySelector('.card-info');

    let ingredients = element.parentNode.querySelectorAll('.active-ingredient');
    let initialPrice = front_info.parentElement.querySelector('h3').textContent.includes('XXL')? 6: 3;
    let initialCal = front_info.parentElement.querySelector('h3').textContent.includes('XXL')? 400: 200;

    let newPrice = 0;
    let newCal = 0;
    let newIngredients = [];
    let text = document.createElement("p");
    text.innerHTML = 'Ingredients: ';
    for (let i=0; i<ingredients.length; i++)
    {
        newPrice += ingredientsMap.get(ingredients[i].textContent).price;
        newCal += ingredientsMap.get(ingredients[i].textContent).cal;
        newIngredients.push(ingredients[i].textContent);
        let span = document.createElement("span");
        span.classList.add('front-ingredient');
        span.innerHTML = ingredients[i].textContent;
        text.appendChild(span);
        text.innerHTML += ', ';
    }

    back_info.querySelector('.price').innerHTML = `<i class="fas fa-dollar-sign"></i> ${(newPrice+initialPrice).toFixed(2)}`;
    back_info.querySelector('.calories').innerHTML = `<i class="fas fa-pizza-slice"></i> ${newCal+initialCal} cal<span class="small">/100g</span>`;

    front_info.querySelector('.price').innerHTML = `<i class="fas fa-dollar-sign"></i> ${(newPrice+initialPrice).toFixed(2)}`;
    front_info.querySelector('.calories').innerHTML = `<i class="fas fa-pizza-slice"></i> ${newCal+initialCal} cal<span class="small">/100g</span>`;

    front_info.parentElement.querySelector('.card-ingredients').innerHTML = text.innerHTML.slice(0,text.innerHTML.length-2);
    if (!front_info.parentElement.querySelector('.card-header').innerHTML.includes('modified'))
    front_info.parentElement.querySelector('.card-header').innerHTML += ' (modified)';
    changeMenuInfo(front_info.parentElement.querySelector('.card-header').textContent, newPrice, newCal, newIngredients);
}


function changeMenuInfo(str, price, cal, arr) {
    str = str.slice(0, str.length-11);
    let temp = pizzaMapTemp.get(str);
    temp.price = price;
    temp.calories = cal;
    temp.ingredients = new Set(arr);
    pizzaMapTemp.set(str,temp);
}